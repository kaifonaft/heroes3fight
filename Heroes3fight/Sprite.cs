﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes3fight
{
    public class Sprite
    {
        public Image Image { get; }
        public Size FrameSize { get; }
        public Point CurrentFrameLocation;

        public Sprite(Size frameSize, Image image)
        {
            FrameSize = frameSize;
            Image = image;
        }

        public virtual Point GetFrameLocationByNum(int num)
        {
            return new Point(0, 0);
        }

        public virtual IEnumerable<Point> FrameLocations()
        {
            yield return new Point(0,0);
        }
    }
}
