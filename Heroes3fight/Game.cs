﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes3fight
{
    public class Game
    {
        private GameField field;
        public bool BlockAllActions = false;

        public Game(GameField field)
        {
            this.field = field;
        }

        public void Move(Point from, Point to)
        {
            BlockAllActions = true;
            Point screenFrom, screenTo;
            screenFrom = CoordsHelper.GameCoordsToDrawCoords(from);
            screenTo = CoordsHelper.GameCoordsToDrawCoords(to);
            AnimationState start, end;
            start = new AnimationState { Position = screenFrom, SpriteNum = 0 };
            end = new AnimationState { Position = screenTo, SpriteNum = 0 };
            var anim = new Animation(start, end, 8, () => { field.Move(from, to); });
            field[from.X, from.Y].Animation = anim;
        }

        public bool SomeAnimationIsRun()
        {
            for (int i = 0; i < field.Height; i++)
                for (int j = 0; j < field.Width; j++)
                    if (field[i, j] != null && field[i, j].Animation!=null && !field[i, j].Animation.IsEnd)
                        return true;
            return false;
        }
    }
}
