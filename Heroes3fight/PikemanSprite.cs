﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes3fight
{
    public class PikemanSprite : Sprite
    {
        public PikemanSprite(Size frameSize, Image image) : base(frameSize, image)
        {
        }

        public override Point GetFrameLocationByNum(int num)
        {
            return new Point(num * FrameSize.Width, 0);
        }

        public override IEnumerable<Point> FrameLocations()
        {
            for (int j = 0; j < 6; j++)
                yield return GetFrameLocationByNum(j);
        }
    }
}
