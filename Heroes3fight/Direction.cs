﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Heroes3fight
{
    public enum Direction
    {
        Right, Up, Left, Down
    }

    public static class DirecitonExtension
    {
        private static Dictionary<Direction, Direction> opposite;
        private static Dictionary<Direction, Point> direction2diff;

        static DirecitonExtension()
        {
            opposite = new Dictionary<Direction, Direction>();
            opposite[Direction.Up] = Direction.Down;
            opposite[Direction.Down] = Direction.Up;
            opposite[Direction.Left] = Direction.Right;
            opposite[Direction.Right] = Direction.Left;

            direction2diff = new Dictionary<Direction, Point>();
            direction2diff[Direction.Right] = new Point(1, 0);
            direction2diff[Direction.Up] = new Point(0, -1);
            direction2diff[Direction.Left] = new Point(-1, 0);
            direction2diff[Direction.Down] = new Point(0, 1);
        }

        public static bool IsOppositeTo(this Direction direction, Direction other)
        {
            return opposite[direction] == other;
        }

        public static Point GetPointDiff(this Direction direction)
        {
            return direction2diff[direction];
        }
    }
}
