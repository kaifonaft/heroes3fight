﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes3fight
{
    public class FightStack : IFieldObject
    {
        public int Speed => 4;
        public bool IsLocking => true;
        public Animation Animation { get; set; }

        public Sprite Sprite { get; set; }

        public void Draw(Graphics g, Point p)
        {
            var pDraw = CoordsHelper.GameCoordsToDrawCoords(p);
            AnimationState state = new AnimationState { Position = pDraw, SpriteNum = 0 };

            if (Animation != null && !Animation.IsEnd)
            {
                state = Animation.Animate();
            }
            var rectTab = new Rectangle(state.Position, Sprite.FrameSize);
            g.DrawImage(
                Sprite.Image,
                rectTab,
                new Rectangle(Sprite.GetFrameLocationByNum(state.SpriteNum), Sprite.FrameSize),
                GraphicsUnit.Pixel);
            g.DrawRectangle(
                new Pen(Color.Red),
                new Rectangle(pDraw, new Size(CoordsHelper.CELL_SIZE, CoordsHelper.CELL_SIZE))
            );
        }
    }
}
