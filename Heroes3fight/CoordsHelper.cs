﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes3fight
{
    public class CoordsHelper
    {
        public const int leftShift = 0, topShift = 0;
        public const int CELL_SIZE = 32;
        public static Point GameCoordsToDrawCoords(Point gamePos)
        {
            int x, y;
            x = gamePos.X * CELL_SIZE+leftShift;
            y = gamePos.Y * CELL_SIZE+topShift;
            return new Point(x,y);
        }

        public static Point DrawCoordsToGameCoords(Point drawCoords)
        {
            int x, y;
            x = (drawCoords.X - leftShift) / CELL_SIZE;
            y = (drawCoords.Y - topShift) / CELL_SIZE;
            return new Point(x, y);
        }
    }
}
