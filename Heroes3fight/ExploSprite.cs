﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes3fight
{
    public class ExploSprite : Sprite
    {
        public ExploSprite(Size frameSize, Image image) : base(frameSize, image)
        {
        }

        public override IEnumerable<Point> FrameLocations()
        {
            int spriteSize = 64;
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                    yield return new Point(j * spriteSize, i * spriteSize);
        }
    }
}
