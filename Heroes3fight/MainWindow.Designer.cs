﻿namespace Heroes3fight
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.debugLabel = new System.Windows.Forms.Label();
            this.animateSprite = new System.Windows.Forms.Button();
            this.moveButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // debugLabel
            // 
            this.debugLabel.AutoSize = true;
            this.debugLabel.Location = new System.Drawing.Point(372, 330);
            this.debugLabel.Name = "debugLabel";
            this.debugLabel.Size = new System.Drawing.Size(37, 13);
            this.debugLabel.TabIndex = 0;
            this.debugLabel.Text = "debug";
            // 
            // animateSprite
            // 
            this.animateSprite.Location = new System.Drawing.Point(404, 49);
            this.animateSprite.Name = "animateSprite";
            this.animateSprite.Size = new System.Drawing.Size(75, 23);
            this.animateSprite.TabIndex = 5;
            this.animateSprite.Text = "animate next";
            this.animateSprite.UseVisualStyleBackColor = true;
            this.animateSprite.Click += new System.EventHandler(this.animateSprite_Click);
            // 
            // moveButton
            // 
            this.moveButton.Location = new System.Drawing.Point(404, 20);
            this.moveButton.Name = "moveButton";
            this.moveButton.Size = new System.Drawing.Size(75, 23);
            this.moveButton.TabIndex = 6;
            this.moveButton.Text = "move";
            this.moveButton.UseVisualStyleBackColor = true;
            this.moveButton.Click += new System.EventHandler(this.moveButton_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 352);
            this.Controls.Add(this.moveButton);
            this.Controls.Add(this.animateSprite);
            this.Controls.Add(this.debugLabel);
            this.Name = "MainWindow";
            this.Text = "Heroes 3 Fight";
            this.Click += new System.EventHandler(this.MainWindow_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label debugLabel;
        private System.Windows.Forms.Button animateSprite;
        private System.Windows.Forms.Button moveButton;
    }
}

