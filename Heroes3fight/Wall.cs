﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes3fight
{
    public class Wall : IFieldObject
    {
        public static Wall One = new Wall();
        public int Speed { get => 0;}

        public bool IsLocking => true;

        public Animation Animation { get; set; }

        public void Draw(Graphics g, Point p)
        {
            var pDraw = CoordsHelper.GameCoordsToDrawCoords(p);
            var rect = new Rectangle(pDraw.X, pDraw.Y, CoordsHelper.CELL_SIZE, CoordsHelper.CELL_SIZE);
            Brush wallBrush = new SolidBrush(Color.Brown);
            g.FillRectangle(wallBrush, rect);
        }
    }
}
