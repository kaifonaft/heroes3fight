﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes3fight
{
    public class Animation
    {
        public AnimationState Start { get; }
        public AnimationState End { get; }
        public int MaxTicks { get; }
        public bool IsEnd
        {
            get { return ticks > MaxTicks; }
        }
        public bool IsFirstRun
        {
            get { return ticks == 0; }
        }

        private int ticks = 0;
        private Action afterEnd;

        public Animation(AnimationState start, AnimationState end, int maxTicks, Action afterEnd = null)
        {
            this.MaxTicks = maxTicks;
            Start = start;
            End = end;
            this.afterEnd = afterEnd;
        }

        public AnimationState Animate()
        {
            int xCur, yCur;
            xCur = (int)Math.Round(Start.Position.X
                + 1.0 * (End.Position.X - Start.Position.X) * ticks / MaxTicks);
            yCur = (int)Math.Round(Start.Position.Y
                + 1.0 * (End.Position.Y - Start.Position.Y) * ticks / MaxTicks);
            ticks++;
            if (IsEnd)
                afterEnd();
            AnimationState state = new AnimationState { SpriteNum = ticks%6, Position = new Point(xCur, yCur) };
            return state;
        }

    }
}
