﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes3fight
{
    public class GameField
    {
        public readonly int Width;
        public readonly int Height;
        private IFieldObject[,] field;

        public GameField(int width, int height)
        {
            this.Width = width;
            this.Height = height;
            field = new IFieldObject[width, height];
        }

        public IFieldObject this[int x, int y]
        {
            get { return field[x, y]; }
            set {
                if(field[x, y] == null)
                    field[x, y] = value;
            }
        }

        public void Move(int fromX, int fromY, int toX, int toY)
        {
            Move(new Point(fromX, fromY), new Point(toX, toY));
        }

        public void Move(Point from, Point to)
        {
            if(field[from.X, from.Y] != null && FreeForMove(field[to.X, to.Y]) && CanMove(from, to))
            {
                field[to.X, to.Y] = field[from.X, from.Y];
                field[from.X, from.Y] = null;
            }
        }

        private bool CanMove(Point from, Point to)
        {
            if (field[from.X, from.Y] is Wall)
                return false;
            var maxLen = field[from.X, from.Y].Speed;
            var q = new Queue<Point>();
            //bool[,] passed = new bool[width, height];
            int[,] len = new int[Width, Height];
            for (int i = 0; i < Width; i++)
                for (int j = 0; j < Height; j++)
                    len[i, j] = -1;
            q.Enqueue(from);
            //passed[from.X, from.Y] = true;
            len[from.X, from.Y] = 0;
            var directions = (Direction[])Enum.GetValues(typeof(Direction));
            while (q.Count > 0)
            {
                var p = q.Dequeue();
                foreach(var dir in directions)
                {
                    var d = dir.GetPointDiff();
                    var t = new Point(p.X+d.X, p.Y+d.Y);
                    if(InFieldPoint(t) && len[t.X, t.Y]==-1 && FreeForMove(field[t.X, t.Y]))
                    {
                        q.Enqueue(t);
                        //passed[t.X, t.Y] = true;
                        len[t.X, t.Y] = len[p.X, p.Y] + 1;
                    }
                }
            }
            if (len[to.X, to.Y] != -1 && len[to.X, to.Y] <= maxLen)
                return true;
            return false;
        }

        private bool InFieldPoint(Point t)
        {
            return t.X >= 0 && t.X < Width && t.Y >= 0 && t.Y < Height;
        }

        private bool FreeForMove(IFieldObject fieldObject)
        {
            return fieldObject == null;
        }
    }
}
