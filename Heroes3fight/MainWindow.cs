﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Heroes3fight
{
    public partial class MainWindow : Form
    {
        private GameField field;
        private Game game;
        private Graphics graphics;
        Brush
            stackBrush = new SolidBrush(Color.Aqua),
            fieldBrush = new SolidBrush(Color.White);
        int cellSize = 32;
        Pen cellPen = new Pen(Color.Gray, 1);
        Point picknerPosition;
        FightStack stack = new FightStack();
        List<Sprite> spriteList;
        Sprite animSprite;
        IEnumerator<Point> animEnum;
        private bool animRun = false;

        public MainWindow()
        {
            InitializeComponent();
            field = new GameField(10, 10);
            var fs = new FightStack();
            fs.Sprite = new PikemanSprite(new Size(128, 128), Image.FromFile("../../Images/PikemanWalk.png"));
            picknerPosition = new Point(0,3);
            field[picknerPosition.X, picknerPosition.Y] = fs;
            field[0, 4] = Wall.One;
            var timer = new Timer();
            timer.Interval = 35;
            timer.Tick += Timer_Tick;
            timer.Start();
            Paint += Draw;
            KeyUp += KeyUpCatch;
            SampleSpriteInit();

            //DoubleBuffered = true;
            graphics = CreateGraphics();
            game = new Game(field);
        }

        private void SampleSpriteInit()
        {
            var pb = new PictureBox();
            Image image;
            image = Image.FromFile("../../Images/Pikeman.png");
            //image = Properties.Resources.Pikeman;
            var fs = new Size(70, 124);
            spriteList = new List<Sprite>();
            var sprite = new Sprite (fs, image);
            spriteList.Add(sprite);
            animSprite = new ExploSprite(new Size(64, 64), Properties.Resources.explosprite);
            animEnum = animSprite.FrameLocations().GetEnumerator();
            spriteList.Add(animSprite);
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (game.SomeAnimationIsRun() || animRun)
                Invalidate();
            if (animRun)
            {
                if (!animEnum.MoveNext())
                    animRun = false;
                else 
                    spriteList[1].CurrentFrameLocation = animEnum.Current;
            }
        }

        private void animateSprite_Click(object sender, EventArgs e)
        {
            animEnum = animSprite.FrameLocations().GetEnumerator();
            spriteList[1].CurrentFrameLocation = animEnum.Current;
            animRun = true;
            Invalidate();
        }

        private void KeyUpCatch(object sender, KeyEventArgs e)
        {
        }

        public void Draw(object sender, EventArgs e)
        {
            graphics.FillRectangle(fieldBrush, new Rectangle(0, 0, field.Width * cellSize, field.Height*cellSize));
            var rect = new Rectangle(0, 0, cellSize, cellSize);
            for (int x = 0; x < field.Width; x++)
                for (int y = 0; y < field.Height; y++)
                {
                    rect.X = x * cellSize;
                    rect.Y = y * cellSize;
                    if (field[x, y] != null)
                    {
                        field[x, y].Draw(graphics, new Point(x, y));
                    } else {
                        //graphics.DrawRectangle(cellPen, rect);
                    }
                }
            if(spriteList != null)
            {
                int x = 0;
                foreach (var sprite in spriteList)
                {
                    DrawSprite(new Point(x, 0), sprite);
                    x += 100;
                }

                var wrapRect = new Rectangle(
                    new Point(-1, -1),
                    new Size(spriteList[0].FrameSize.Width + 2, spriteList[0].FrameSize.Height + 2));
                graphics.DrawRectangle(cellPen, wrapRect);
            }
        }

        private void moveButton_Click(object sender, EventArgs e)
        {
            var from = picknerPosition;
            var to = new Point(1, 3);
            game.Move(from, to);
        }

        private void MainWindow_Click(object sender, EventArgs e)
        {
            var mouseArgs = (MouseEventArgs)e;
            string mes = "";
            Point gameLoc = new Point();
            if (PointInsideField(mouseArgs.X, mouseArgs.Y))
            {
                mes = "inside field";
                gameLoc = CoordsHelper.DrawCoordsToGameCoords(mouseArgs.Location);
                game.Move(picknerPosition, gameLoc);
                picknerPosition = gameLoc;
            }
            this.debugLabel.Text = string.Format("X:{0} Y:{1} {2} {3}", mouseArgs.X, mouseArgs.Y, mes, gameLoc);
        }

        private bool PointInsideField(int x, int y)
        {
            int pixelWidth, pixelHeight;
            pixelWidth = field.Width * CoordsHelper.CELL_SIZE;
            pixelHeight = field.Height * CoordsHelper.CELL_SIZE;
            return x <= pixelWidth && y <= pixelHeight;
        }

        public void DrawSprite(Point p, Sprite sprite)
        {
            var rectTab = new Rectangle(p, sprite.FrameSize);
            graphics.FillRectangle(fieldBrush, rectTab);
            graphics.DrawImage(
                sprite.Image,
                rectTab,
                new Rectangle(sprite.CurrentFrameLocation, sprite.FrameSize),
                GraphicsUnit.Pixel);
        }
    }
}
