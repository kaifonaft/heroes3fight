﻿using System.Drawing;

namespace Heroes3fight
{
    public interface IFieldObject
    {
        Animation Animation { get; set; }
        int Speed { get; } 
        bool IsLocking { get; }
        void Draw(Graphics g, Point p);
    }
}