﻿namespace SpriteHelper
{
    partial class SpriteHelpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.spriteTop = new System.Windows.Forms.NumericUpDown();
            this.spriteLeft = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.spriteVertJumpSize = new System.Windows.Forms.TextBox();
            this.spriteJumpUp = new System.Windows.Forms.Button();
            this.spriteJumpDown = new System.Windows.Forms.Button();
            this.spriteHorizJumpSize = new System.Windows.Forms.TextBox();
            this.spriteJumpLeft = new System.Windows.Forms.Button();
            this.spriteJumpRight = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spriteTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spriteLeft)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.spriteTop);
            this.groupBox1.Controls.Add(this.spriteLeft);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.spriteVertJumpSize);
            this.groupBox1.Controls.Add(this.spriteJumpUp);
            this.groupBox1.Controls.Add(this.spriteJumpDown);
            this.groupBox1.Controls.Add(this.spriteHorizJumpSize);
            this.groupBox1.Controls.Add(this.spriteJumpLeft);
            this.groupBox1.Controls.Add(this.spriteJumpRight);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(325, 169);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(278, 161);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "sprite config";
            // 
            // spriteTop
            // 
            this.spriteTop.Location = new System.Drawing.Point(72, 68);
            this.spriteTop.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.spriteTop.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.spriteTop.Name = "spriteTop";
            this.spriteTop.Size = new System.Drawing.Size(51, 20);
            this.spriteTop.TabIndex = 16;
            this.spriteTop.ValueChanged += new System.EventHandler(this.spriteTop_ValueChanged);
            // 
            // spriteLeft
            // 
            this.spriteLeft.Location = new System.Drawing.Point(72, 31);
            this.spriteLeft.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.spriteLeft.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.spriteLeft.Name = "spriteLeft";
            this.spriteLeft.Size = new System.Drawing.Size(51, 20);
            this.spriteLeft.TabIndex = 15;
            this.spriteLeft.ValueChanged += new System.EventHandler(this.spriteLeft_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(165, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "<< >>";
            // 
            // spriteVertJumpSize
            // 
            this.spriteVertJumpSize.Location = new System.Drawing.Point(165, 67);
            this.spriteVertJumpSize.Name = "spriteVertJumpSize";
            this.spriteVertJumpSize.Size = new System.Drawing.Size(38, 20);
            this.spriteVertJumpSize.TabIndex = 13;
            this.spriteVertJumpSize.Text = "128";
            // 
            // spriteJumpUp
            // 
            this.spriteJumpUp.Location = new System.Drawing.Point(36, 66);
            this.spriteJumpUp.Name = "spriteJumpUp";
            this.spriteJumpUp.Size = new System.Drawing.Size(30, 23);
            this.spriteJumpUp.TabIndex = 12;
            this.spriteJumpUp.Text = "<<";
            this.spriteJumpUp.UseVisualStyleBackColor = true;
            this.spriteJumpUp.Click += new System.EventHandler(this.spriteJumpUp_Click);
            // 
            // spriteJumpDown
            // 
            this.spriteJumpDown.Location = new System.Drawing.Point(129, 65);
            this.spriteJumpDown.Name = "spriteJumpDown";
            this.spriteJumpDown.Size = new System.Drawing.Size(30, 23);
            this.spriteJumpDown.TabIndex = 11;
            this.spriteJumpDown.Text = ">>";
            this.spriteJumpDown.UseVisualStyleBackColor = true;
            this.spriteJumpDown.Click += new System.EventHandler(this.spriteJumpDown_Click);
            // 
            // spriteHorizJumpSize
            // 
            this.spriteHorizJumpSize.Location = new System.Drawing.Point(165, 31);
            this.spriteHorizJumpSize.Name = "spriteHorizJumpSize";
            this.spriteHorizJumpSize.Size = new System.Drawing.Size(38, 20);
            this.spriteHorizJumpSize.TabIndex = 10;
            this.spriteHorizJumpSize.Text = "62";
            // 
            // spriteJumpLeft
            // 
            this.spriteJumpLeft.Location = new System.Drawing.Point(36, 29);
            this.spriteJumpLeft.Name = "spriteJumpLeft";
            this.spriteJumpLeft.Size = new System.Drawing.Size(30, 23);
            this.spriteJumpLeft.TabIndex = 9;
            this.spriteJumpLeft.Text = "<<";
            this.spriteJumpLeft.UseVisualStyleBackColor = true;
            this.spriteJumpLeft.Click += new System.EventHandler(this.spriteJumpLeft_Click);
            // 
            // spriteJumpRight
            // 
            this.spriteJumpRight.Location = new System.Drawing.Point(129, 29);
            this.spriteJumpRight.Name = "spriteJumpRight";
            this.spriteJumpRight.Size = new System.Drawing.Size(30, 23);
            this.spriteJumpRight.TabIndex = 8;
            this.spriteJumpRight.Text = ">>";
            this.spriteJumpRight.UseVisualStyleBackColor = true;
            this.spriteJumpRight.Click += new System.EventHandler(this.spriteJumpRight_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "left";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "top";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 342);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spriteTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spriteLeft)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown spriteTop;
        private System.Windows.Forms.NumericUpDown spriteLeft;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox spriteVertJumpSize;
        private System.Windows.Forms.Button spriteJumpUp;
        private System.Windows.Forms.Button spriteJumpDown;
        private System.Windows.Forms.TextBox spriteHorizJumpSize;
        private System.Windows.Forms.Button spriteJumpLeft;
        private System.Windows.Forms.Button spriteJumpRight;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

