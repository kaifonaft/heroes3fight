﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Heroes3fight;

namespace SpriteHelper
{
    public partial class SpriteHelpForm : Form
    {
        Sprite sprite;
        Graphics graphics;
        private Brush fieldBrush = new SolidBrush(Color.White);
        private Pen cellPen = new Pen(Color.Gray, 1);

        public SpriteHelpForm()
        {
            InitializeComponent();
            graphics = CreateGraphics();
            SpriteInit();
            Paint += Draw;
        }

        private void Draw(object sender, PaintEventArgs e)
        {
            DrawSprite(new Point(0, 0), sprite);
        }

        public void DrawSprite(Point p, Sprite sprite)
        {
            var rectTab = new Rectangle(p, sprite.FrameSize);
            graphics.FillRectangle(fieldBrush, rectTab);
            graphics.DrawImage(
                sprite.Image,
                rectTab,
                new Rectangle(sprite.CurrentFrameLocation, sprite.FrameSize),
                GraphicsUnit.Pixel);
            var wrapRect = new Rectangle(
                new Point(-1, -1),
                new Size(sprite.FrameSize.Width + 2, sprite.FrameSize.Height + 2));
            graphics.DrawRectangle(cellPen, wrapRect);
        }

        private void SpriteInit()
        {
            Image image = Image.FromFile("../../../Heroes3fight/Images/PikemanWalk.png");
            //image = Properties.Resources.Pikeman;
            var fs = new Size(128, 128);
            sprite = new Sprite(fs, image);
            spriteHorizJumpSize.Text = fs.Width.ToString();
            spriteVertJumpSize.Text = fs.Height.ToString();
        }

        private void spriteTopTextBox_TextChanged(object sender, EventArgs e)
        {
            int top;
            var tb = (TextBox)sender;
            if (int.TryParse(tb.Text, out top))
                sprite.CurrentFrameLocation = new Point(sprite.CurrentFrameLocation.X, top);
        }

        private void SpriteHorizontalMove(int diff)
        {
            int left = sprite.CurrentFrameLocation.X + diff;
            spriteLeft.Text = left.ToString();
        }

        private void SpriteVerticalMove(int diff)
        {
            int top = sprite.CurrentFrameLocation.Y + diff;
            spriteTop.Text = top.ToString();
        }

        private void spriteJumpRight_Click(object sender, EventArgs e)
        {
            int jumpSize = 10;
            if (int.TryParse(spriteHorizJumpSize.Text, out jumpSize))
                SpriteHorizontalMove(jumpSize);
        }

        private void spriteJumpLeft_Click(object sender, EventArgs e)
        {
            int jumpSize = 10;
            if (int.TryParse(spriteHorizJumpSize.Text, out jumpSize))
                SpriteHorizontalMove(-jumpSize);
        }

        private void spriteJumpDown_Click(object sender, EventArgs e)
        {
            int jumpSize = 10;
            if (int.TryParse(spriteVertJumpSize.Text, out jumpSize))
                SpriteVerticalMove(jumpSize);
        }

        private void spriteJumpUp_Click(object sender, EventArgs e)
        {
            int jumpSize = 10;
            if (int.TryParse(spriteVertJumpSize.Text, out jumpSize))
                SpriteVerticalMove(-jumpSize);
        }

        private void spriteLeft_ValueChanged(object sender, EventArgs e)
        {
            var num = (NumericUpDown)sender;
            sprite.CurrentFrameLocation = new Point((int)num.Value, sprite.CurrentFrameLocation.Y);
            Invalidate();
        }

        private void spriteTop_ValueChanged(object sender, EventArgs e)
        {
            var num = (NumericUpDown)sender;
            sprite.CurrentFrameLocation = new Point(sprite.CurrentFrameLocation.X, (int)num.Value);
            Invalidate();
        }
    }
}
