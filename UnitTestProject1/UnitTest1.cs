﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Heroes3fight;
using System.Drawing;

namespace UnitTestHeroes3
{
    [TestClass]
    public class UnitTestGameField
    {
        private static Wall wall = new Wall();
        private static FightStack stack = new FightStack();
        private static FightStack stack2 = new FightStack();

        [TestInitialize]
        public void TestInitialize()
        {

        }

        [TestMethod]
        public void TestIndexException() {
            int w, h;
            w = 10;
            h = 10;
            var game = new GameField(w,h);
            Assert.IsNull(game[0, 0]);
            Assert.ThrowsException<IndexOutOfRangeException>(() => game[w,h]);
            Assert.IsNull(game[w-1, h-1]);
        }

        [TestMethod]
        public void TestAddStack()
        {
            var game = new GameField(10, 10);
            game[3,3] = stack;
            Assert.IsInstanceOfType(game[3, 3], typeof(FightStack));
            Assert.AreEqual(stack, game[3,3]);
        }

        [TestMethod]
        public void TestAddStackToTheSamePoint()
        {
            var game = new GameField(10, 10);
            game[3, 3] = stack;
            Assert.IsInstanceOfType(game[3, 3], typeof(FightStack));
            Assert.AreEqual(stack, game[3,3]);
            game[3, 3] = stack2;
            Assert.AreEqual(stack, game[3, 3]);
        }

        [TestMethod]
        public void TestMoveFieldObject()
        {
            var game = new GameField(10,10);
            game[3, 3] = stack;
            game.Move(3,3, 5,4);
            Assert.IsNull(game[3,3]);
            Assert.AreEqual(stack, game[5,4]);
        }

        [TestMethod]
        public void TestMovePoint()
        {
            var game = new GameField(10, 10);
            game[3, 3] = stack;
            var fromPoint = new Point(3, 3);
            var toPoint = new Point(5, 3);
            game.Move(fromPoint, toPoint);
            Assert.IsNull(game[3, 3]);
            Assert.AreEqual(stack, game[5,3]);
        }

        [TestMethod]
        public void TestMoveToOccupied()
        {
            var game = new GameField(10, 10);
            game[3, 3] = stack;
            game[5, 5] = stack2;
            var fromPoint = new Point(3, 3);
            var toPoint = new Point(5, 5);
            game.Move(3, 3, 5, 5);
            Assert.AreNotEqual(stack, game[5, 5]);
            Assert.AreEqual(stack, game[3, 3]);
            Assert.AreEqual(stack2, game[5, 5]);
        }

        [TestMethod]
        public void TestWall()
        {
            var game = new GameField(10, 10);
            game[3, 3] = stack;
            game[5, 5] = wall;
            game.Move(3, 3, 5, 5);
            Assert.AreEqual(stack, game[3, 3]);
            Assert.AreEqual(wall, game[5, 5]);
            game.Move(5, 5, 6, 6);
            Assert.AreEqual(wall, game[5, 5]);
            Assert.IsNull(game[6, 6]);
        }

        [TestMethod]
        public void TestMoveSpeed()
        {
            var game = new GameField(10, 10);
            game[0, 3] = stack;
            var fromPoint = new Point(0, 3);
            var toPoint = new Point(5, 3);
            game.Move(fromPoint, toPoint);
            Assert.IsNull(game[5, 3]);
            Assert.AreEqual(stack, game[0, 3]);
        }

        [TestMethod]
        public void TestMoveSpeed2()
        {
            var game = new GameField(10, 10);
            game[0, 3] = stack;
            var fromPoint = new Point(0, 3);
            var toPoint = new Point(3, 4);
            game.Move(fromPoint, toPoint);
            Assert.IsNull(game[0, 3]);
            Assert.AreEqual(stack, game[3, 4]);
        }

        [TestMethod]
        public void TestMoveSpeedWall()
        {
            var game = new GameField(10, 10);
            game[0, 0] = stack;
            game[0, 1] = wall;
            game[1, 1] = wall;
            var fromPoint = new Point(0, 0);
            var toPoint = new Point(0, 2);
            game.Move(fromPoint, toPoint);
            Assert.AreEqual(stack, game[0, 0]);
            Assert.IsNull(game[0, 2]);
        }

        [TestMethod]
        public void TestMoveSpeedWallAndStack()
        {
            var game = new GameField(10, 10);
            game[0, 0] = stack;
            game[0, 1] = wall;
            game[1, 1] = stack2;
            var fromPoint = new Point(0, 0);
            var toPoint = new Point(0, 2);
            game.Move(fromPoint, toPoint);
            Assert.AreEqual(stack, game[0, 0]);
            Assert.IsNull(game[0, 2]);
        }

        [TestMethod]
        public void TestMoveAnimation()
        {
            var game = new GameField(10, 10);
            game[0, 0] = stack;
            var fromPoint = new Point(0, 0);
            var toPoint = new Point(1, 0);
            game.Move(fromPoint, toPoint);
            Assert.AreEqual(stack, game[1, 0]);
            Assert.IsNull(game[0, 0]);
        }
    }
}
